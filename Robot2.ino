#define RED 2
#define GREEN 3
#define BLUE 4

#define DROIT 0
#define DROITE 1
#define GAUCHE 2

const float COEFF = .92; // coefficient de pondération de la moyenne

typedef struct Color
{
  int r, g, b;
};

typedef struct Mesure
{
  Color c[3];
};

#define blanc_sur_noir  0
#define bordure         1

#define MODE  0

Mesure m0; // mesure de calibrage
float mm[3][3]; // mesure moyenne

void setup()
{
  Serial.begin(115200);
  // put your setup code here, to run once:
  pinMode(13, OUTPUT); // internal LED
  pinMode(RED, OUTPUT);
  digitalWrite(RED, HIGH);
  pinMode(GREEN, OUTPUT);
  digitalWrite(GREEN, HIGH);
  pinMode(BLUE, OUTPUT);
  digitalWrite(BLUE, HIGH);
  pinMode(5, OUTPUT);
  digitalWrite(5, HIGH);
  pinMode(6, OUTPUT);
  digitalWrite(6, HIGH);
  pinMode(7, OUTPUT);
  digitalWrite(7, HIGH);
  pinMode(8, OUTPUT);
  digitalWrite(8, HIGH);
  //
  int i, j;
  for (i = 0; i < 3; i++)
  {
    for (j = 0; j < 3; j++)
    {
      mm[i][j] = 0.;
    }
  }
  delay(100);
  m0 = mesure();
}

void avance ()
{
  digitalWrite(5, LOW);
  digitalWrite(8, LOW);
  digitalWrite(6, HIGH);
  digitalWrite(7, HIGH);
}

void stop ()
{
  digitalWrite(5, HIGH);
  digitalWrite(6, HIGH);
  digitalWrite(7, HIGH);
  digitalWrite(8, HIGH);
}

void gauche (bool strong)
{
  digitalWrite(6, !strong);
  digitalWrite(8, LOW);
  digitalWrite(5, HIGH);
  digitalWrite(7, HIGH);
}

void droite (bool strong)
{
  digitalWrite(5, LOW);
  digitalWrite(7, !strong);
  digitalWrite(6, HIGH);
  digitalWrite(8, HIGH);
}

Mesure mesure ()
{
  Mesure m;
  // mesures analogiques
  digitalWrite(RED, LOW); // turn on red
  delay(5);
  m.c[0].r = analogRead(A0);
  m.c[1].r = analogRead(A1);
  m.c[2].r = analogRead(A2);
  digitalWrite(RED, HIGH); // turn off red
  digitalWrite(GREEN, LOW); // turn on green
  delay(5);
  m.c[0].g = analogRead(A0);
  m.c[1].g = analogRead(A1);
  m.c[2].g = analogRead(A2);
  digitalWrite(GREEN, HIGH);
  digitalWrite(BLUE, LOW);
  delay(5);
  m.c[0].b = analogRead(A0);
  m.c[1].b = analogRead(A1);
  m.c[2].b = analogRead(A2);
  digitalWrite(BLUE, HIGH);
  return m;
}

void printColor(Color c)
{
  Serial.print("(");
  Serial.print(c.r);
  Serial.print(",");
  Serial.print(c.g);
  Serial.print(",");
  Serial.print(c.b);
  Serial.print(")");
}

Color diff (Color a, Color b)
{
  Color r;
  r.r = a.r - b.r;
  r.g = a.g - b.g;
  r.b = a.b - b.b;
  return r;
}

uint32_t sqr(int x)
{
  return ((uint32_t)x) * ((uint32_t)x);
}

// compute the distance between the two colors
uint32_t d (Color a, Color b)
{
  return sqr(a.r - b.r) + sqr(a.g - b.g) + sqr(a.b - b.b);
}

int norm1(Color c)
{
  return abs(c.r) + abs(c.g) + abs(c.b);
}

int rd = 0; // précédente décision
int mode = 0;

void loop()
{
  Mesure m = mesure();
  /*
    Serial.print("color1: ");
    printColor(m.c[0]);
    Serial.print(", color2: ");
    printColor(m.c[1]);
    Serial.print(", color3: ");
    printColor(m.c[2]);
    Serial.println();
  */
  //
  int i;
  for (i = 0; i < 3; i++)
    m.c[i] = diff(m.c[i], m0.c[i]);
  //
  Serial.println("Apres etalonage");
  Serial.print("color1: ");
  printColor(m.c[0]);
  Serial.print(", color2: ");
  printColor(m.c[1]);
  Serial.print(", color3: ");
  printColor(m.c[2]);
  Serial.println();

  Mesure dif;
  int sdiff = 0;

  for (i = 0; i < 3; i++)
  {
    dif.c[i].r = m.c[i].r - mm[i][0];
    dif.c[i].g = m.c[i].g - mm[i][1];
    dif.c[i].b = m.c[i].b - mm[i][2];
    sdiff += (abs(dif.c[i].r) + abs(dif.c[i].g) + abs(dif.c[i].b));
    if (!mode)
    {
      mm[i][0] = mm[i][0] * COEFF + (1. - COEFF) * (float)m.c[i].r;
      mm[i][1] = mm[i][1] * COEFF + (1. - COEFF) * (float)m.c[i].g;
      mm[i][2] = mm[i][2] * COEFF + (1. - COEFF) * (float)m.c[i].b;
    }
  }

  Serial.print("mm1: (");
  for (i = 0; i < 3; i++)
  {
    Serial.print(mm[0][i]);
    if (i < 2)
      Serial.print(", ");
  }
  Serial.print("), mm2: (");
  for (i = 0; i < 3; i++)
  {
    Serial.print(mm[1][i]);
    if (i < 2)
      Serial.print(", ");
  }
  Serial.print("), mm3: (");
  for (i = 0; i < 3; i++)
  {
    Serial.print(mm[2][i]);
    if (i < 2)
      Serial.print(", ");
  }
  Serial.println(")");


#if MODE == blanc_sur_noir
  int sm = norm1(m.c[1]);

  if (sdiff < 64 && sm > 300)
  {
    Serial.print("sdiff = ");
    Serial.println(sdiff);
    //avance
    avance();
    rd = DROIT;
  } else
  {
    /*
      uint32_t d1 = d(m.c[0], m.c[1]);
      uint32_t d2 = d(m.c[1], m.c[2]);
      uint32_t d3 = d(m.c[0], m.c[2]);
    */
    int sd = norm1(m.c[0]);
    int sg = norm1(m.c[2]);
    /*
      Serial.print("d1 = ");
      Serial.print(d1);
      Serial.print(", d2 = ");
      Serial.print(d2);
      Serial.print(", d3 = ");
      Serial.print(d3);
      Serial.println();
    */
    Serial.print("sd = ");
    Serial.print(sd);
    Serial.print(", sm = ");
    Serial.print(sm);
    Serial.print(", sg = ");
    Serial.print(sg);
    Serial.println();
    bool strong = (sg + sd + sm > 2000);
    //if (d2 < 4000 && rd != DROITE)
    if (sg > 150 && sg > sd && 2 * sg > sm) // && rd != DROITE)
    {
      Serial.println("gauche !");
      // tourne à gauche
      gauche(strong);
      //delay(sg/50);
      if (strong)
        delay(1 + sg / 128);
      else
      {
        delay(sg / 64);
        if (sg < 600)
        {
          avance();
          delay(6 - sg / 100);
        }
      }
      stop();
      rd = GAUCHE;
    }//else if (d1 < 4000 && rd != GAUCHE)
    else if (sd > 150 && sd > sg && 2 * sd > sm)
    {
      // tourne à droite
      droite(strong);
      //delay(sd/50);
      if (strong)
        delay(1 + sd / 128);
      else
      {
        delay(sd / 64);
        if (sd < 600)
        {
          avance();
          delay(6 - sd / 100);
        }
      }
      stop();
      rd = DROITE;
    } else
    {
      if (sm > 150 && sg < 250 && sd < 250 && sdiff < 400)
      {
        // avance
        avance();
        //delay(20);
        //stop();
        rd = DROIT;
      } else if (sg > 200 && sd > 200)
      {
        // c'est peut-être une intersection
        avance();
        delay(4);
        gauche(false);
        delay(4);
        stop();
      } else if (sm > 300)
      {
        avance();
        delay(10);
        stop();
      } else
      {
        stop();
      }
    }
    //
    //delay(200);
  }
#elif MODE == bordure
  int nd = norm1(dif.c[0]);
  int ng = norm1(dif.c[2]);
  int ed = norm1(diff(m.c[0], m.c[1]));
  int eg = norm1(diff(m.c[2], m.c[1]));
  int em = norm1(diff(m.c[0], m.c[2]));
  Serial.print("nd = ");
  Serial.print(nd);
  Serial.print(", ng = ");
  Serial.println(ng);
  Serial.print("ed = ");
  Serial.print(ed);
  Serial.print(", eg = ");
  Serial.print(eg);
  Serial.print(", em = ");
  Serial.println(em);
  if ((em < 100 && ed < 100 && eg < 100) || (mode && em < 100 && ed < 100 && eg < 100))
  { // les écarts entre les trois couleurs sont faibles
    if (ng > 100 && ng > nd || mode == 1)
    {
      mode = 1;
      gauche(true);
      delay(20);
      stop();
      return;
    } else if (nd > 100 || mode == 2)
    {
      mode = 2;
      droite(true);
      delay(20);
      stop();
      return;
    }
  }
  //if (em > 100)
  {
    mode = 0;
    if (abs(ed - eg) > 50)
    {
      if (ed < eg)
      {
        // tourne à gauche
        gauche(false);
        delay(8);
        stop();
      } else
      {
        // tourne à droite
        droite(false);
        delay(8);
        stop();
      }
    } else
    {
      if (nd < 50 && ng < 50)
      {
        // les moyennes aux bords sont presque inchangées -> avance vite
        avance();
      } else
      {
        // avance prudemment
        avance();
        delay(10);
        stop();
      }
    }
  }
  //delay(200);
#endif
}
